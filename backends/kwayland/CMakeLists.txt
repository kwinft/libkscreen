find_package(KF5 ${KF5_MIN_VERSION} REQUIRED COMPONENTS
    CoreAddons
)

set(wayland_SRCS
    waylandbackend.cpp
    waylandconfig.cpp
    wayland_interface.cpp
    waylandoutput.cpp
    waylandscreen.cpp
    ../utils.cpp
)

ecm_qt_declare_logging_category(
    wayland_SRCS
    HEADER wayland_logging.h
    IDENTIFIER KSCREEN_WAYLAND
    CATEGORY_NAME org.kde.kscreen.wayland
)

qt5_add_dbus_interface(wayland_SRCS org.kde.KWin.TabletModeManager.xml tabletmodemanager_interface)

add_library(KSC_KWayland MODULE ${wayland_SRCS})

add_subdirectory(plugins)

set_target_properties(KSC_KWayland PROPERTIES LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin/kf5/kscreen")
set_target_properties(KSC_KWayland PROPERTIES PREFIX "")
target_link_libraries(KSC_KWayland Qt5::Core
                                  Qt5::DBus
                                  Qt5::Gui
                                  KF5::Screen
                                  KF5::WaylandClient
                                  KF5::CoreAddons
)

install(TARGETS KSC_KWayland DESTINATION ${KDE_INSTALL_PLUGINDIR}/kf5/kscreen/)
