/*************************************************************************
Copyright © 2020 Roman Gilg <subdiff@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**************************************************************************/
#include "wrapland_output.h"

#include "waylandbackend.h"
#include "waylandconfig.h"
#include "../utils.h"

#include <mode.h>
#include <edid.h>

#include "../../wayland_logging.h"

#include <Wrapland/Client/output_configuration_v1.h>
#include <Wrapland/Client/output_device_v1.h>

using namespace KScreen;
namespace Wl = Wrapland::Client;

const QMap<Wl::OutputDeviceV1::Transform, Output::Rotation>
s_rotationMap = {
    {Wl::OutputDeviceV1::Transform::Normal, Output::None},
    {Wl::OutputDeviceV1::Transform::Rotated90, Output::Right},
    {Wl::OutputDeviceV1::Transform::Rotated180, Output::Inverted},
    {Wl::OutputDeviceV1::Transform::Rotated270, Output::Left},
    {Wl::OutputDeviceV1::Transform::Flipped, Output::None},
    {Wl::OutputDeviceV1::Transform::Flipped90, Output::Right},
    {Wl::OutputDeviceV1::Transform::Flipped180, Output::Inverted},
    {Wl::OutputDeviceV1::Transform::Flipped270, Output::Left}
};

Output::Rotation toKScreenRotation(const Wl::OutputDeviceV1::Transform transform)
{
    auto it = s_rotationMap.constFind(transform);
    return it.value();
}

Wl::OutputDeviceV1::Transform toWraplandTransform(const Output::Rotation rotation)
{
    return s_rotationMap.key(rotation);
}

WraplandOutput::WraplandOutput(quint32 id, QObject *parent)
    : WaylandOutput(id, parent)
    , m_device(nullptr)
{
}

bool WraplandOutput::enabled() const
{
    return m_device != nullptr;
}

QByteArray WraplandOutput::edid() const
{
    return m_device->edid();
}

QRectF WraplandOutput::geometry() const
{
    return m_device->geometry();
}

Wrapland::Client::OutputDeviceV1 *WraplandOutput::outputDevice() const
{
    return m_device;
}

void WraplandOutput::createOutputDevice(Wl::Registry *registry, quint32 name, quint32 version)
{
    Q_ASSERT(!m_device);
    m_device = registry->createOutputDeviceV1(name, version);

    connect(m_device, &Wl::OutputDeviceV1::removed, this, &WraplandOutput::removed);
    connect(m_device, &Wl::OutputDeviceV1::done, this, [this]() {
                disconnect(m_device, &Wl::OutputDeviceV1::done, this, nullptr);
                connect(m_device, &Wl::OutputDeviceV1::changed, this, &WraplandOutput::changed);
                Q_EMIT dataReceived();
    });
}

void WraplandOutput::updateKScreenOutput(OutputPtr &output)
{
    // Initialize primary output
    output->setEnabled(m_device->enabled() == Wl::OutputDeviceV1::Enablement::Enabled);
    output->setConnected(true);
    output->setPrimary(true); // FIXME: wayland doesn't have the concept of a primary display
    output->setName(name());
    output->setSizeMm(m_device->physicalSize());
    output->setPos(m_device->geometry().topLeft().toPoint());
    output->setRotation(s_rotationMap[m_device->transform()]);

    ModeList modeList;
    QStringList preferredModeIds;
    m_modeIdMap.clear();
    QString currentModeId = QStringLiteral("-1");

    for (const Wl::OutputDeviceV1::Mode &wlMode : m_device->modes()) {
        ModePtr mode(new Mode());
        const QString name = modeName(wlMode);

        QString modeId = QString::number(wlMode.id);
        if (modeId.isEmpty()) {
            qCDebug(KSCREEN_WAYLAND) << "Could not create mode id from"
                                     << wlMode.id << ", using" << name << "instead.";
            modeId = name;
        }

        if (m_modeIdMap.contains(modeId)) {
            qCWarning(KSCREEN_WAYLAND) << "Mode id already in use:" << modeId;
        }
        mode->setId(modeId);

        // Wrapland gives the refresh rate as int in mHz
        mode->setRefreshRate(wlMode.refreshRate / 1000.0);
        mode->setSize(wlMode.size);
        mode->setName(name);

        if (wlMode.flags.testFlag(Wl::OutputDeviceV1::Mode::Flag::Current)) {
            currentModeId = modeId;
        }
        if (wlMode.flags.testFlag(Wl::OutputDeviceV1::Mode::Flag::Preferred)) {
            preferredModeIds << modeId;
        }

        // Update the kscreen => Wrapland mode id translation map
        m_modeIdMap.insert(modeId, wlMode.id);
        // Add to the modelist which gets set on the output
        modeList[modeId] = mode;
    }

    if (currentModeId == QLatin1String("-1")) {
        qCWarning(KSCREEN_WAYLAND) << "Could not find the current mode id" << modeList;
    }

    output->setCurrentModeId(currentModeId);
    output->setPreferredModes(preferredModeIds);
    output->setModes(modeList);
//    output->setScale(m_device->scaleF());
    output->setType(Utils::guessOutputType(m_device->model(), m_device->model()));
}

bool WraplandOutput::setWlConfig(Wl::OutputConfigurationV1 *wlConfig,
                                const KScreen::OutputPtr &output)
{
    bool changed = false;

    // enabled?
    if ((m_device->enabled() == Wl::OutputDeviceV1::Enablement::Enabled)
            != output->isEnabled()) {
        changed = true;
        const auto enablement = output->isEnabled() ? Wl::OutputDeviceV1::Enablement::Enabled :
                                                      Wl::OutputDeviceV1::Enablement::Disabled;
        wlConfig->setEnabled(m_device, enablement);
    }

    // position
    if (m_device->geometry().topLeft() != output->pos()) {
        changed = true;
        wlConfig->setGeometry(m_device, QRectF(output->pos(), m_device->geometry().size()));
    }

//    // scale
//    if (!qFuzzyCompare(m_device->scaleF(), output->scale())) {
//        changed = true;
//        wlConfig->setScaleF(m_device, output->scale());
//    }

    // rotation
    if (toKScreenRotation(m_device->transform()) != output->rotation()) {
        changed = true;
        wlConfig->setTransform(m_device, toWraplandTransform(output->rotation()));
    }

    // mode
    if (m_modeIdMap.contains(output->currentModeId())) {
        const int newModeId = m_modeIdMap.value(output->currentModeId(), -1);
        if (newModeId != m_device->currentMode().id) {
            changed = true;
            wlConfig->setMode(m_device, newModeId);
        }
    } else {
        qCWarning(KSCREEN_WAYLAND) << "Invalid kscreen mode id:" << output->currentModeId()
                                   << "\n\n" << m_modeIdMap;
    }

    // logical size
    if (m_device->geometry().size() != output->logicalSize()) {
        QSizeF size = output->explicitLogicalSize();
        if (!size.isValid()) {
            size = output->logicalSize();
        }
        changed = true;
        wlConfig->setGeometry(m_device, QRectF(output->pos(), size));
    }
    return changed;
}

QString WraplandOutput::modeName(const Wl::OutputDeviceV1::Mode &m) const
{
    return QString::number(m.size.width()) + QLatin1Char('x') +
           QString::number(m.size.height()) + QLatin1Char('@') +
           QString::number(qRound(m.refreshRate/1000.0));
}

QString WraplandOutput::name() const
{
    Q_ASSERT(m_device);
    return QStringLiteral("%1 %2").arg(m_device->manufacturer(), m_device->model());
}

QDebug operator<<(QDebug dbg, const WraplandOutput *output)
{
    dbg << "WraplandOutput(Id:" << output->id() <<", Name:" << \
        QString(output->outputDevice()->manufacturer() + QLatin1Char(' ') + \
        output->outputDevice()->model())  << ")";
    return dbg;
}
