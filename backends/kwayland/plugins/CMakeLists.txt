add_subdirectory(kwayland)

find_package(Wrapland CONFIG)
set_package_properties(Wrapland PROPERTIES
    TYPE OPTIONAL
    PURPOSE "Used as Wrapper library for Wayland protocol objects."
)

if (Wrapland_FOUND)
    message("Wrapland found with version ${Wrapland_VERSION}.")
    if (${Wrapland_VERSION} VERSION_GREATER_EQUAL "0.518.80")
        message("Selecting Wrapland KWinFT and wlroots plugins.")
        add_subdirectory(wrapland-wlr)
    else()
        message("Selecting Wrapland KWinFT plugin (Wrapland version too low for wlroots plugin).")
    endif()
    add_subdirectory(wrapland)
endif()
