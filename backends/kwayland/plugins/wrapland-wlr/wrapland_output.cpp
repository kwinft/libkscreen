/*************************************************************************
Copyright © 2020 Roman Gilg <subdiff@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**************************************************************************/
#include "wrapland_output.h"

#include "wrapland_interface.h"

#include "waylandbackend.h"
#include "waylandconfig.h"
#include "../utils.h"

#include <mode.h>
#include <edid.h>

#include "../../wayland_logging.h"

#include <Wrapland/Client/wlr_output_configuration_v1.h>

using namespace KScreen;
namespace Wl = Wrapland::Client;

const QMap<Wl::WlrOutputHeadV1::Transform, Output::Rotation>
s_rotationMap = {
    {Wl::WlrOutputHeadV1::Transform::Normal, Output::None},
    {Wl::WlrOutputHeadV1::Transform::Rotated90, Output::Right},
    {Wl::WlrOutputHeadV1::Transform::Rotated180, Output::Inverted},
    {Wl::WlrOutputHeadV1::Transform::Rotated270, Output::Left},
    {Wl::WlrOutputHeadV1::Transform::Flipped, Output::None},
    {Wl::WlrOutputHeadV1::Transform::Flipped90, Output::Right},
    {Wl::WlrOutputHeadV1::Transform::Flipped180, Output::Inverted},
    {Wl::WlrOutputHeadV1::Transform::Flipped270, Output::Left}
};

Output::Rotation toKScreenRotation(const Wl::WlrOutputHeadV1::Transform transform)
{
    auto it = s_rotationMap.constFind(transform);
    return it.value();
}

Wl::WlrOutputHeadV1::Transform toWraplandTransform(const Output::Rotation rotation)
{
    return s_rotationMap.key(rotation);
}

WraplandOutput::WraplandOutput(quint32 id, Wrapland::Client::WlrOutputHeadV1 *head,
                               WraplandInterface *parent)
    : WaylandOutput(id, parent)
    , m_head(head)
{
    connect(m_head, &Wl::WlrOutputHeadV1::changed, this, &WraplandOutput::changed);
    connect(m_head, &Wl::WlrOutputHeadV1::removed, this, &WraplandOutput::removed);


    auto manager = parent->outputManager();
    connect(manager, &Wl::WlrOutputManagerV1::done,
        this, [this, manager]() {
            disconnect(manager, &Wl::WlrOutputManagerV1::done, this, nullptr);
            Q_EMIT dataReceived();
        }
    );
}

bool WraplandOutput::enabled() const
{
    return m_head != nullptr;
}

QByteArray WraplandOutput::edid() const
{
    // wlroots protocol does not provide edid information.
    return QByteArray();
}

bool portraitMode(Wrapland::Client::WlrOutputHeadV1 *head)
{
    auto transform = head->transform();
    return transform == Wl::WlrOutputHeadV1::Transform::Rotated90
            || transform == Wl::WlrOutputHeadV1::Transform::Rotated270
            || transform == Wl::WlrOutputHeadV1::Transform::Flipped90
            || transform == Wl::WlrOutputHeadV1::Transform::Flipped270;
}

QRectF WraplandOutput::geometry() const
{
    auto modeSize = m_head->currentMode()->size();

    // Rotate and scale.
    if (portraitMode(m_head)) {
        modeSize.transpose();
    }

    modeSize = modeSize / m_head->scale();

    return QRectF(m_head->position(), modeSize);
}

Wrapland::Client::WlrOutputHeadV1 *WraplandOutput::outputHead() const
{
    return m_head;
}

QString modeName(const Wl::WlrOutputModeV1 *mode)
{
    return QString::number(mode->size().width()) + QLatin1Char('x') +
           QString::number(mode->size().height()) + QLatin1Char('@') +
           QString::number( qRound(mode->refresh() / 1000.0) );
}

void WraplandOutput::updateKScreenOutput(OutputPtr &output)
{
    // Initialize primary output
    output->setEnabled(m_head->enabled());
    output->setConnected(true);
    output->setPrimary(true); // FIXME: wayland doesn't have the concept of a primary display
    output->setName(name());
    output->setSizeMm(m_head->physicalSize());
    output->setPos(m_head->position());
    output->setRotation(s_rotationMap[m_head->transform()]);

    ModeList modeList;
    QStringList preferredModeIds;
    m_modeIdMap.clear();
    QString currentModeId = QStringLiteral("-1");

    auto currentMode = m_head->currentMode();

    int modeCounter = 0;
    for (auto wlMode : m_head->modes()) {
        const auto modeId = QString::number(++modeCounter);

        ModePtr mode(new Mode());

        mode->setId(modeId);

        // Wrapland gives the refresh rate as int in mHz.
        mode->setRefreshRate(wlMode->refresh() / 1000.0);
        mode->setSize(wlMode->size());
        mode->setName(modeName(wlMode));

        if (wlMode->preferred()) {
            preferredModeIds << modeId;
        }
        if (currentMode == wlMode) {
            currentModeId = modeId;
        }

        // Update the KScreen => Wrapland mode id translation map.
        m_modeIdMap.insert(modeId, wlMode);

        // Add to the modelist which gets set on the output.
        modeList[modeId] = mode;
    }

    if (currentModeId == QLatin1String("-1")) {
        qCWarning(KSCREEN_WAYLAND) << "Could not find the current mode id" << modeList;
    }

    output->setCurrentModeId(currentModeId);
    output->setPreferredModes(preferredModeIds);
    output->setModes(modeList);
    output->setScale(m_head->scale());
    output->setType(Utils::guessOutputType(m_head->name(), m_head->name()));
}

bool WraplandOutput::setWlConfig(Wl::WlrOutputConfigurationV1 *wlConfig,
                                 const KScreen::OutputPtr &output)
{
    bool changed = false;

    // enabled?
    if (m_head->enabled() != output->isEnabled()) {
        changed = true;
    }

    // In any case set the enabled state to initialize the output's native handle.
    wlConfig->setEnabled(m_head, output->isEnabled());

    // position
    if (m_head->position() != output->pos()) {
        changed = true;
        wlConfig->setPosition(m_head, output->pos());
    }

    // scale
    if (!qFuzzyCompare(m_head->scale(), output->scale())) {
        changed = true;
        wlConfig->setScale(m_head, output->scale());
    }

    // rotation
    if (toKScreenRotation(m_head->transform()) != output->rotation()) {
        changed = true;
        wlConfig->setTransform(m_head, toWraplandTransform(output->rotation()));
    }

    // mode
    if (m_modeIdMap.contains(output->currentModeId())) {
        auto newMode = m_modeIdMap.value(output->currentModeId(), nullptr);
        if (newMode != m_head->currentMode()) {
            changed = true;
            wlConfig->setMode(m_head, newMode);
        }
    } else {
        qCWarning(KSCREEN_WAYLAND) << "Invalid kscreen mode id:" << output->currentModeId()
                                   << "\n\n" << m_modeIdMap;
    }

    return changed;
}

QString WraplandOutput::name() const
{
    Q_ASSERT(m_head);
    return m_head->description();
}

QDebug operator<<(QDebug dbg, const WraplandOutput *output)
{
    dbg << "WraplandOutput(Id:" << output->id() <<", Name:" << \
        QString(output->outputHead()->name() + QLatin1Char(' ') + \
        output->outputHead()->description())  << ")";
    return dbg;
}
