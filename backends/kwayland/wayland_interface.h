/*************************************************************************
Copyright © 2020   Roman Gilg <subdiff@gmail.com>

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**************************************************************************/
#pragma once

#include "config.h"

#include <QEventLoop>
#include <QObject>
#include <QVector>

class QThread;

namespace KScreen
{
class Output;
class WaylandOutput;
class WaylandScreen;

class WaylandInterface : public QObject
{
    Q_OBJECT

public:
    enum class Type {
        KWayland = 0,
        Wrapland,
    };

    ~WaylandInterface() override;

    virtual void initConnection(QThread *thread) = 0;
    virtual bool isInitialized() const;

    // Compositor side names as keys
    virtual QMap<int, WaylandOutput*> outputMap() const = 0;

    virtual void applyConfig(const KScreen::ConfigPtr &newConfig);
    virtual void updateConfig(KScreen::ConfigPtr &config) = 0;

Q_SIGNALS:
    void configChanged();
    void initialized();
    void connectionFailed(const QString &socketName);
    void outputsChanged();

protected:
    explicit WaylandInterface(QObject *parent = nullptr);

    virtual void insertOutput(WaylandOutput *output) = 0;
    virtual WaylandOutput* takeOutput(WaylandOutput *output) = 0;

    bool signalsBlocked() const;
    void blockSignals();
    void unblockSignals();

    void checkInitialized();

    void addOutput(WaylandOutput *output);
    virtual void handleDisconnect();

private:
    void removeOutput(WaylandOutput *output);

    /**
     * Finalize: when the output is is initialized, we put it in the known outputs map,
     * remove it from the list of initializing outputs, and emit configChanged().
     */
    virtual void initOutput(WaylandOutput *output);

    void tryPendingConfig();

    // Compositor side names
    QList<WaylandOutput*> m_initializingOutputs;
    int m_lastOutputId = -1;

    bool m_blockSignals;
    KScreen::ConfigPtr m_kscreenConfig;
    WaylandScreen *m_screen;
};

class WaylandFactory : public QObject
{
    Q_OBJECT
public:
    WaylandFactory(QObject *parent = nullptr) : QObject(parent) {}
    ~WaylandFactory() override = default;

    virtual WaylandInterface* createInterface(QObject *parent = nullptr) = 0;
};

}

Q_DECLARE_INTERFACE(KScreen::WaylandFactory, "org.kde.libkscreen.waylandinterface")
